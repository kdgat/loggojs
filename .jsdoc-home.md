# Loggo
#### (Pronounced Log-Go)
### Simple Transport based and extendable Javascript Logger

Yet another logging library focused on simplicity and extensibility.

## But why?
Because I can. And because current Logging implementations felt a little too overengineered for me.

## Features
* **Simple** Get to logging in just 3 (4 if you count the require) lines of code.
* **Transport based Logging**
    * Console Transport: Log to the NodeJS console or Devtools.
* **Multiple Logger instances** Different Transports and log levels for each instance.
* **Global Logging instance** Only need quick logger functions? Use the global logger.
* **Easy to write new Transports** Write a class that extends `BaseTransport` and define a log method.
* **Linked Transport Children** Have a transport you like the way it's set up? Create a child that is directly linked to its parent transport. Changes on the parent affect the child if the child doesn't overwrite the change!
* **Queued** Calling logging functions adds the message to a queue.
* **Async** Logging functions return a promise that resolves when the queue is empty and all messages are logged.
* **Familiar Syntax** Exposes the same logging functions as `console` and can be used as a drop in replacement.
* **Custom Log Levels** Extend the existing log levels with custom named ones.

## Installation
```sh
npm install @kdg/loggojs
```

## Usage
It's recommended that you create a new Logger instance. It's as simple as calling `new`!
```js
// Create a new Logger
const lg = new Logger("source");
```
Now that this is done, don't forget to set the level of your transports, otherwise they are disabled!
```js
// Sets the transport level for this logger transport
// Possible values are "TRACE", "DEBUG", "INFO", "WARNING", "ERROR" or any numeric value
// See the values of Logger.Levels for all named levels
lg.transports.console.level = "INFO"
```
We are now ready to log messages!
```js
lg.info("Hello World!");
> "[Info][source]Hello World!"
```

Or use the global Logger:
```js
Logger.transports.console.level = "DEBUG";
Logger.warn("Watch out!");
> "[Warning][GLOBAL]Watch out!"
```
The global logger uses the globally defined transports internally.
### Available methods
```js
lg.trace(message);  // Level: Trace (0)
lg.debug(message);  // Level: Debug (1)
lg.info(message);   // Level: Info (2)
lg.warn(message);   // Level: Warning (3)
lg.error(message);  // Level: Error (4)
lg.log(message);    // Level: -1
```
## Transport Options
### Base Transport Options
All Transports share these options.
```js
// Get or set the log level of the current Transporter
// Set to `false` to disable transport
// Available levels: "TRACE", "DEBUG", "INFO", "WARNING", "ERROR" or any numeric value
Transport.level;
> false
Transport.level = "WARNING";
Transport.level;
> 3
```
```js
// Get or set the message format of the Transporter output
// The following variables are available and string-format is used to insert them
// Text: {level}, {text}, {source}
// Time: {y}, {m}, {d}, {h}, {i}, {s}, {ms}, {z}
Transport.format;
> "[{level}][{source}]{text}"
```
### Console Transport
See **Base Transport Options**
