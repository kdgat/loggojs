"use strict";

/* globals describe, before, it */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const capture = require("./utils.spec.js").capture;
const randomString = require("./utils.spec.js").randomString;

describe("Local Logger", async function () {
    before(require("./utils.spec.js").before);

    let logger = null;
    it("Creates a Logger Instance", function () {
        const Logger = require("../");

        logger = new Logger("Local");

        expect(logger, "New Logger is not a Logger Instance").to.be.instanceOf(Logger);
    });

    it("Doesn't log anything because the Transport is disabled", async function () {
        if (!logger) this.skip();

        const testString = randomString(255);

        let stdout = await capture(logger.info.bind(logger), testString);

        expect(stdout, "Logger has logged the following: " + stdout).to.equal("");
    });
    it("Logs with enabled transport", async function () {
        if (!logger) this.skip();

        logger.transports.console.level = "Info";

        const testString = randomString(255);

        let stdout = await capture(logger.info.bind(logger), testString);

        expect(stdout.trim(), "Logger has logged wrong String").to.equal("[Info][Local]" + testString);
    });
    it("Doesn't log because log level is too high", async function () {
        if (!logger) this.skip();

        logger.transports.console.level = "Warning";

        const testString = randomString(255);

        let stdout = await capture(logger.info.bind(logger), testString);

        expect(stdout.trim(), "Logger has logged wrong String").to.equal("");
    });

    it("Creates a Logger Instance without source and logs correctly", async function () {
        const Logger = require("../");

        const logger = new Logger();

        logger.transports.console.level = "Info";

        const testString = randomString(255);

        let stdout = await capture(logger.info.bind(logger), testString);

        expect(stdout.trim(), "Logger has logged wrong String").to.equal("[Info][unset]" + testString);
    });

    it("Logs all Loglevels correctly", async function () {
        if (!logger) this.skip();

        const tests = {
            "-1": logger.log.bind(logger),
            "Trace": logger.trace.bind(logger),
            "Debug": logger.debug.bind(logger),
            "Info": logger.info.bind(logger),
            "Warning": logger.warn.bind(logger),
            "Error": logger.error.bind(logger)
        };

        for (let [loglevel, fn] of Object.entries(tests)) {
            logger.transports.console.level = loglevel;
            const testString = randomString(255);

            let test = await capture(fn, testString);

            expect(test.split("\n")[0].trim(), "Wrong Log Output for loglevel " + loglevel).to.eql((loglevel === "Trace" ? "Trace: " : "") + "[" + loglevel + "][Local]" + testString);
        }
    });
});
