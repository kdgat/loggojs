"use strict";

/* globals describe, before, it */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;

describe("Loglevel Resolution", async function () {
    before(require("./utils.spec").before);

    it("Doesn't load the base transport class as global Extension", function () {
        const Logger = require("../lib/Logger");
        const Transport = require("../lib/Transports/BaseTransport");

        expect(Logger.use.bind(Logger, Transport), "Registration of Base Transport successful").to.throw();
    });

    it("Throws error on logging", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();

        expect(t.log.bind(t), "Doesn't error out").to.throw();
    });

    it("Allows changing format", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();

        let oldFormat = t.format;

        expect(oldFormat).to.eql("[{level}][{source}]{text}");

        expect(t.formatMessage(2, "UnitTest", "Hello World")).to.eql("[Info][UnitTest]Hello World");

        t.format = "[{source}]{text}";
        expect(t.formatMessage(2, "UnitTest", "Hello World")).to.eql("[UnitTest]Hello World");
    });

    it("Returns properly formatted timezone offset", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();
        t.format = "{z}";

        const oldGetTimezoneOffset = Date.prototype.getTimezoneOffset;

        Date.prototype.getTimezoneOffset = function () {
            return -180;
        };
        expect(t.formatMessage(2, "UnitTest", "Hello World")).to.eql("+3");
        Date.prototype.getTimezoneOffset = function () {
            return 180;
        };
        expect(t.formatMessage(2, "UnitTest", "Hello World")).to.eql("-3");

        Date.prototype.getTimezoneOffset = oldGetTimezoneOffset;
    });

    it("Returns the correct loglevel", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();

        expect(t.level).to.eql(false);
        t.level = "INFO";
        expect(t.level).to.eql(2);
        t.level = "INVALID";
        expect(t.level).to.eql(2);
    });

    it("Uses a function to format the log message", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();

        t.format = function (formatObj) {
            return "FROMFN:" + formatObj.text;
        };
        expect(t.formatMessage(3, "UnitTest", "Hello World")).to.eql("FROMFN:Hello World");
    });
});
