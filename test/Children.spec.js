"use strict";

/* globals describe, before, it */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const randomString = require("./utils.spec.js").randomString;

describe("Children relationships", async function () {
    before(require("./utils.spec").before);

    it("Creates Children with same class as parent", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        const t = new Transport();

        expect(t).to.be.instanceOf(Transport);

        let c1 = t.createChild();
        let c2 = c1.createChild();

        expect(c1).to.be.instanceOf(Transport);
        expect(c2).to.be.instanceOf(Transport);
    });
    it("Confirm link of children to parent", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        const t = new Transport();

        let c1 = t.createChild();
        let c2 = c1.createChild();

        expect(c2.level).to.eql(false);

        t.level = 3;
        expect(c2.level, "Parent change did not propergate down to children").to.eql(3);

        c1.level = 1;
        expect(t.level, "Child change bubbled up to parent").to.eql(3);
        expect(c2.level, "Parent change did not propergate down to children").to.eql(1);

        delete c1["level"];
        expect(c1.level).to.eql(3);
        expect(c2.level).to.eql(3);

        t.format = function (formObj) {
            return formObj.text;
        };
        expect(c2.formatMessage(2, "UnitTest", "Hello World")).to.eql("Hello World");
    });
    it("Keeps getters and setters", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        const t = new Transport();

        let c1 = t.createChild();

        c1.level = "INFO";
        expect(c1.level).to.eql(2);
    });
    it("Reset format by deleting", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();

        let c1 = t.createChild();

        c1.format = function (formObj) {
            return formObj.text;
        };
        expect(c1.formatMessage(3, "UnitTest", "Hello World")).to.eql("Hello World");

        delete c1["format"];
        expect(t.formatMessage(3, "UnitTest", "Hello World")).to.eql("[Warning][UnitTest]Hello World");
    });
    it("Can assign/delete regular properties", function () {
        const Transport = require("../lib/Transports/BaseTransport");
        let t = new Transport();

        let c1 = t.createChild();

        let testStr = randomString(255);
        c1.testVal = testStr;

        expect(c1.testVal).to.eql(testStr);

        delete c1["testVal"];
        expect(c1.testVal).to.be.undefined;
    });
});
