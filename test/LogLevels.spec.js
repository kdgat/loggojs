"use strict";

/* globals describe, before, it */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;

describe("Loglevel Resolution", function () {
    before(require("./utils.spec").before);

    it("Resolves Loglevels correctly", function () {
        const Level = require("../lib/LogLevels");

        expect(Level.resolve(10), "Number Loglevel resolution failed").to.eql(10);
        expect(Level.resolve("3"), "Numeric string Loglevel resolution failed").to.eql(3);
        expect(Level.resolve(false), "Boolean Loglevel resolution failed").to.eql(false);
        expect(Level.resolve("off"), "'Off' string resolution failed").to.eql(false);
        expect(Level.resolve("Info"), "Named full Loglevel resolution failed").to.eql(2);
        expect(Level.resolve("D", "Named first letter Loglevel resolution failed")).to.eql(1);
        expect(Level.resolve("Warn", "Named startsWith Loglevel resolution failed")).to.eql(3);
        expect(Level.resolve("JeffreyPetterson"), "Invalid named Loglevel resolution succeeded").to.eql(null);
        expect(Level.resolve(undefined), "Undefined Loglevel resolution succeeded").to.eql(null);
        expect(Level.resolve(null), "null Loglevel resolution succeeded").to.eql(null);
    });
});
