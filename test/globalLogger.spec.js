"use strict";

/* globals describe, before, it */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const capture = require("./utils.spec.js").capture;
const randomString = require("./utils.spec.js").randomString;

describe("Global Logger", async function () {
    before(require("./utils.spec.js").before);
    it("Loads Logger Class", function () {
        const Logger = require("../lib/Logger");
        expect(Logger.transports, "Global transports is not an empty array!").to.eql({});
        expect(Logger.levels, "Unexpected Loglevels Array").to.eql(["Trace", "Debug", "Info", "Warning", "Error"]);
        expect(Logger.levels, "No valid resolve function on the Loglevels Array").to.have.property("resolve").that.is.a("function");
    });

    let loadedConsoleTransport = false;

    it("Registers new Transport", function () {
        const ConsoleTransport = require("../lib/Transports/ConsoleTransport");
        const Logger = require("../lib/Logger");

        Logger.use(ConsoleTransport);
        expect(Logger.transports, "ConsoleTransport not registered properly").to.have.property("console").that.is.instanceOf(ConsoleTransport);

        loadedConsoleTransport = true;
    });
    it("Doesn't log anything because the Transport is disabled", async function () {
        if (!loadedConsoleTransport) this.skip();

        const Logger = require("../lib/Logger");

        const testString = randomString(255);

        let stdout = await capture(Logger.info, testString);

        expect(stdout, "Logger has logged the following: " + stdout).to.equal("");
    });
    it("Doesn't log because transport loglevel is too high", async function () {
        if (!loadedConsoleTransport) this.skip();

        const Logger = require("../lib/Logger");

        Logger.transports.console.level = "Warning";

        const testString = randomString(255);

        let stdout = await capture(Logger.info, testString);

        expect(stdout.trim(), "Logger has logged wrong String").to.equal("");
    });
    it("Logs with enabled transport", async function () {
        if (!loadedConsoleTransport) this.skip();

        const Logger = require("../lib/Logger");

        Logger.transports.console.level = "Info";

        const testString = randomString(255);

        let stdout = await capture(Logger.info, testString);

        expect(stdout.trim(), "Logger has logged wrong String").to.equal("[Info][GLOBAL]" + testString);
    });

    it("Logs a custom loglevel by calling logMessage", async function () {
        if (!loadedConsoleTransport) this.skip();
        const Logger = require("../lib/Logger");

        // Generate random test level
        let testLevel = Math.ceil(Math.random() * 20) + 10;

        Logger.transports.console.level = 10;

        const testString = randomString(255);

        let stdout = await capture(Logger.logMessage.bind(Logger, testLevel), testString);

        expect(stdout.trim(), "Logger has logged wrong String").to.equal("[" + testLevel + "][GLOBAL]" + testString);

        Logger.transports.console.level = -1;
        stdout = await capture(Logger.logMessage.bind(Logger, undefined), testString);
        expect(stdout.trim(), "Logger has logged wrong String").to.equal("[Info][GLOBAL]" + testString);
    });

    it("Logs all Loglevels correctly", async function () {
        if (!loadedConsoleTransport) this.skip();
        const Logger = require("../lib/Logger");

        const tests = {
            "-1": Logger.log,
            "Trace": Logger.trace,
            "Debug": Logger.debug,
            "Info": Logger.info,
            "Warning": Logger.warn,
            "Error": Logger.error
        };

        for (let [loglevel, fn] of Object.entries(tests)) {
            Logger.transports.console.level = loglevel;
            const testString = randomString(255);

            let test = await capture(fn, testString);

            expect(test.split("\n")[0].trim(), "Wrong Log Output for loglevel " + loglevel).to.eql((loglevel === "Trace" ? "Trace: " : "") + "[" + loglevel + "][GLOBAL]" + testString);
        }
    });
});
