"use strict";

/* globals describe, before, it */
/* eslint-disable no-console */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const capture = require("./utils.spec.js").capture;
const sleep = require("./utils.spec.js").sleep;
const randomString = require("./utils.spec.js").randomString;

describe("Async logging", async function () {
    before(require("./utils.spec").before);

    it("Register slow Transport", function () {
        const BaseTransport = require("../lib/Transports/BaseTransport");
        const Logger = require("../lib/Logger");

        class UnitSlowTransport extends BaseTransport {
            static __register() {
                return {
                    type: "transport",
                    name: "unitSlowTransport"
                };
            }

            async log(level, sources, message) {
                await sleep(500);
                console.log(message);
            }
        }


        Logger.use(UnitSlowTransport);
    });

    it("Queue and log multiple messages", async function () {
        this.timeout(0);
        const Logger = require("../lib/Logger");

        Logger.transports.unitSlowTransport.level = -1;

        const stringsArr = [];
        for (let i = 0; i <= 25; i++) stringsArr.push(randomString(64));

        let stdout = await capture(async function () {
            let allPromises = [];
            stringsArr.forEach(msg => allPromises.push(Logger.log(msg)));
            await Promise.all(allPromises);
        }, "");

        expect(stdout).to.eql(stringsArr.join("\n"));
    });
});
