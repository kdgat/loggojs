"use strict";

/* globals describe, before, it */

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const expect = chai.expect;
const capture = require("./utils.spec.js").capture;

describe("Extensions", async function () {
    before(require("./utils.spec").before);

    it("Doesn't load the base extension class", function () {
        const ExtensionClass = require("../lib/Extension");
        const Logger = require("../lib/Logger");

        expect(Logger.use.bind(Logger, ExtensionClass), "Loads Base Extension Class").to.throw();
    });

    it("Loads a custom Extension class", function () {
        const Extension = require("../lib/Extension");
        const Logger = require("../lib/Logger");

        class UnitExtension extends Extension {
            static get __register() {
                return {
                    type: "unit_default",
                    name: "Unit Default Fallback"
                };
            }
        }

        expect(Logger.use(UnitExtension), "Threw an error during registration").to.eql(false);
    });

    it("Doesn't load a Transport Extension twice", function () {
        const BaseTransport = require("../lib/Transports/BaseTransport");
        const Logger = require("../lib/Logger");

        class UnitTransport extends BaseTransport {
            static get __register() {
                return {
                    type: "transport",
                    name: "unitTransport"
                };
            }
        }

        expect(Logger.use(UnitTransport), "Threw an error during registration").to.eql(true);
        expect(Logger.use(UnitTransport), "Threw an error during registration").to.eql(false);

        Logger.transports.unitTransport.level = false;
    });

    it("Won't log because of canLog", async function () {
        const ConsoleTransport = require("../lib/Transports/ConsoleTransport");
        const Logger = require("../lib/Logger");

        class UnitConsoleTransport extends ConsoleTransport {
            canLog() {
                return false;
            }

            static get __register() {
                return {
                    type: "transport",
                    name: "unitConsoleTransport"
                };
            }
        }

        Logger.use(UnitConsoleTransport);

        Logger.transports.unitConsoleTransport.level = "DEBUG";
        let stdout = await capture(Logger.info, "This is a test");

        expect(stdout).to.eql("");
    });
});
