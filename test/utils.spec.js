"use strict";
const capcon = require("capture-console");

module.exports.before = function () {
    delete require.cache[require.resolve("../lib/Extension")];
    delete require.cache[require.resolve("../lib/Logger")];
    delete require.cache[require.resolve("../lib/LogLevels")];
    delete require.cache[require.resolve("../lib/Transports/BaseTransport")];
    delete require.cache[require.resolve("../lib/Transports/ConsoleTransport")];
};

module.exports.capture = async function capture(fn, msg, intercept = true) {
    let stdout = "";
    let stderr = "";
    (intercept ? capcon.startIntercept : capcon.startCapture)(process.stdout, function (data) {
        stdout += data;
    });
    (intercept ? capcon.startIntercept : capcon.startCapture)(process.stderr, function (data) {
        stderr += data;
    });
    await fn(msg);
    (intercept ? capcon.stopIntercept : capcon.stopCapture)(process.stdout);
    (intercept ? capcon.stopIntercept : capcon.stopCapture)(process.stderr);

    return stdout.trim() || stderr.trim();
};

module.exports.randomString = function randomString(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
};

module.exports.sleep = function (time = 1000) {
    return new Promise((resolve) => {
        setTimeout(resolve, time);
    });
};
