"use strict";
const LOGLEVELS = require("./LogLevels");
const resolveLevel = LOGLEVELS.resolve;


const Transports = new WeakMap();
const queue = new WeakMap();
const sources = new WeakMap();

let globalTransports = {};
let globalLogger = null;


/**
 * Class that represents a single logger.<br>
 * **Important:** All logging functions have a static counterpart that access the global logger.
 *
 * @param {string} [source="unset"] The Source region this logger is used in
 * @class Logger
 */
class Logger {
    constructor(source = "unset") {
        let globalTransportProxies = {};

        for (let [name, transport] of Object.entries(globalTransports)) {
            globalTransportProxies[name] = transport.createChild();
        }

        let transports = Object.assign({}, globalTransportProxies);
        Transports.set(this, transports);
        queue.set(this, []);
        sources.set(this, source);
    }


    /**
     * Return all transports assigned to this logger
     *
     * @readonly
     * @returns {Object} An object with Name-Transport Instance pairs
     * @memberof Logger
     */
    get transports() {
        return Object.assign({}, Transports.get(this));
    }


    /**
     * Log a message to all assigned transports
     *
     * @async
     * @param {string|number} [level="INFO"] The log level
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    logMessage(level = "INFO", ...message) {
        return (async () => {
            let q = queue.get(this);
            let logMsg = async () => {
                let d = q[0];
                let logLevel = resolveLevel(d.level);
                for (let transport of Object.values(this.transports)) {
                    if (transport.level === false) continue;
                    if (typeof transport.canLog === "function" && !transport.canLog()) continue;
                    if (transport.level > logLevel) continue;
                    await transport.log(logLevel, sources.get(this), ...d.message);
                }
                q.shift();
                if (q.length > 0) await logMsg();
            };
            q.push({
                level,
                message
            });
            if (q.length === 1) await logMsg();
        })();
    }
    static logMessage(level, ...message) {
        return globalLogger.logMessage(level, ...message);
    }

    /**
     * Log a message at _TRACE_ (0) Loglevel
     *
     * @async
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    trace(...message) {
        return this.logMessage("TRACE", ...message);
    }
    static trace(...message) {
        return globalLogger.trace(...message);
    }
    /**
     * Log a message at _DEBUG_ (1) Loglevel
     *
     * @async
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    debug(...message) {
        return this.logMessage("DEBUG", ...message);
    }
    static debug(...message) {
        return globalLogger.debug(...message);
    }
    /**
     * Log a message at _INFO_ (2) Loglevel
     *
     * @async
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    info(...message) {
        return this.logMessage("INFO", ...message);
    }
    static info(...message) {
        return globalLogger.info(...message);
    }
    /**
     * Log a message at _WARNING_ (3) Loglevel
     *
     * @async
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    warn(...message) {
        return this.logMessage("WARNING", ...message);
    }
    static warn(...message) {
        return globalLogger.warn(...message);
    }
    /**
     * Log a message at _ERROR_ (4) Loglevel
     *
     * @async
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    error(...message) {
        return this.logMessage("ERROR", ...message);
    }
    static error(...message) {
        return globalLogger.error(...message);
    }
    /**
     * Log a message at -1 Loglevel
     *
     * @async
     * @param {*} message The text of the log message
     * @returns {Promise} Promise that resolves as soon as the log message queue is empty
     * @memberof Logger
     */
    log(...message) {
        return this.logMessage(-1, ...message);
    }
    static log(...message) {
        return globalLogger.log(...message);
    }


    /**
     * Return an array of available named loglevels<br>
     * Add, remove or reorganize loglevels in this Array to change the available log level names.
     * See [levels](module-levels.html)
     *
     * @return {string[]} The loglevel array and resolve function
     * @readonly
     * @static
     * @memberof Logger
     */
    static get levels() {
        return LOGLEVELS;
    }

    /**
     * Register new extensions to Logger.<br>
     * Currently only supports Transports.
     *
     * @static
     * @param {ExtensionClass} extensionClass Class, that extends Loggo in some shape or form
     * @returns {boolean} True on successful registering, false otherwise
     * @memberof Logger
     */
    static use(extensionClass) {
        let {
            name,
            type
        } = typeof extensionClass.__register === "function" ? extensionClass.__register() : extensionClass.__register;

        switch (type.toLowerCase()) {
            case "transport":
                if (globalTransports[name]) return false;
                globalTransports[name] = Transports.get(globalLogger)[name] = new extensionClass;
                return true;
            default:
                return false;
        }
    }

    /**
     * Access the globally registered Transporters
     *
     * @readonly
     * @returns {Object} An object with Name-Transport Instance pairs
     * @static
     * @memberof Logger
     */
    static get transports() {
        return Object.assign({}, globalTransports);
    }
}

globalLogger = new Logger("GLOBAL");

module.exports = Logger;
