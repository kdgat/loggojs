"use strict";

/**
 * @typedef {Object} RegistrationMetadata
 * @global
 * @property {string} type The type of the extension
 * @property {string} name The name of the extension
 */

/**
 * A class that serves as the base for all Logger extension classes
 *
 * @class ExtensionClass
 * @abstract
 */
class ExtensionClass {
    /**
     * Return the metadata that is required to register an extension for Logger
     *
     * @virtual
     * @static
     * @returns {RegistrationMetadata}
     * @memberof ExtensionClass
     */
    static __register() {
        throw new Error("Override the __register() function in your Extension Class");
    }
}

module.exports = ExtensionClass;
