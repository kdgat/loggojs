"use strict";

const format = require("string-format");

const LOGLEVELS = require("../LogLevels");
const resolveLevel = LOGLEVELS.resolve;
const ExtensionClass = require("../Extension");

const TransportFormat = new WeakMap();
const TransportLevel = new WeakMap();

/**
 *
 *
 * @class BaseTransport
 * @alias Transport
 * @abstract
 * @param {string|number|boolean} [level=false] The log level that the Transporter is going to be instantiated with
 * @param {string|function} [format="[{level}][{source}]{text}"] The log message format of messages
 * @extends {ExtensionClass}
 */
class BaseTransport extends ExtensionClass {
    constructor(level = false, format = "[{level}][{source}]{text}") {
        super();
        TransportLevel.set(this, level);
        TransportFormat.set(this, format);
    }

    /**
     * Get or set the level that the Transporter logs messages for.<br>
     * The log level is resolved to a number internally and returned as such.<br>
     * Only loglevels equal or higher than this level will get printed out.
     *
     * @memberof BaseTransport
     */
    get level() {
        let level = TransportLevel.get(this);
        return level;
    }
    set level(val) {
        if (resolveLevel(val) !== null) TransportLevel.set(this, resolveLevel(val));
    }
    __levelDelete() {
        TransportLevel.delete(this);
    }

    /**
     * The format that the log message will be printed out as.<br>
     * This can either be a string with variables or a function that returns a log string.
     *
     * @memberof BaseTransport
     */
    get format() {
        return TransportFormat.get(this);
    }
    set format(val) {
        TransportFormat.set(this, typeof val !== "function" ? (val + "") + "" : val);
    }
    __formatDelete() {
        TransportFormat.delete(this);
    }


    /**
     * Take in the data required to form a finished log message string and return it
     *
     * @param {string|number} level The log level that is going to be potentially inserted into the string
     * @param {string} source The source that the logger has been instantiated with
     * @param {string} data The message itself
     * @returns {string} The formatted string
     * @memberof BaseTransport
     */
    formatMessage(level, source, data) {
        let date = new Date();

        let frmtObj = {
            level: LOGLEVELS[level] ? LOGLEVELS[level] : level,
            text: data,
            source,
            y: date.getFullYear(),
            m: date.getMonth() + 1,
            d: date.getDate(),
            h: date.getHours(),
            i: date.getMinutes(),
            s: date.getSeconds(),
            ms: date.getMilliseconds(),
            z: (() => {
                let offset = date.getTimezoneOffset() / -60;
                if (offset > 0) return "+" + offset;
                else return offset;
            })()
        };

        let frmtStr = this.format;
        if (typeof frmtStr === "function") return frmtStr(frmtObj);
        return format(frmtStr, frmtObj);
    }


    /**
     * Runtime check before logging.
     *
     * @returns {boolean} Wether or not a log should happen with this transport
     * @memberof BaseTransport
     */
    canLog() {
        return true;
    }

    /**
     * The core function of a transport. This function is responsible to log the message to its destination.<br>
     * If the logging process is async, return a promise that resolve when the log has reached its target.<br>
     * Every transport needs to implement this function
     *
     * @async
     * @virtual
     * @param {string|number} level The log level of this message
     * @param {string} source The source that the logger has been instantiated with
     * @param {*} message The data you wish to be logged
     * @returns {Promise|undefined}
     * @memberof BaseTransport
     */
    log() {
        throw new Error("Override the log() function in your custom Transport");
    }


    /**
     * Creates a directly linked child.<br>
     * Directly linked children reflect their parents' property changes unless the child has a property overriding the parent.
     *
     * @returns Instance of {@link BaseTransport}
     * @memberof BaseTransport
     */
    createChild() {
        function getDescriptor(obj, key) {
            try {
                return Object.getOwnPropertyDescriptor(obj, key) || getDescriptor(Object.getPrototypeOf(obj), key);
            } catch (error) {
                return;
            }
        }

        let self = this;
        let pTarget = {
            createChild: self.createChild
        };
        let handler = {};
        let proxy = new Proxy(pTarget, handler);
        handler.get = function (target, prop) {
            let ret = prop in target && typeof target[prop] !== "undefined" ? target[prop] : self[prop];
            if (typeof ret === "function")
                return ret.bind(
                    prop in pTarget && typeof pTarget[prop] !== "undefined" ?
                    proxy :
                    self
                );
            else return ret;
        };
        handler.set = function (target, prop, val) {
            let desc = getDescriptor(target, prop);
            if (desc && desc.set && typeof desc.set === "function") desc.set.call(target, val);
            target[prop] = val;
            return true;
        };
        handler.deleteProperty = function (target, prop) {
            if (typeof target["__" + prop + "Delete"] === "function") {
                target["__" + prop + "Delete"]();
                return true;
            }
            return delete target[prop];
        };
        Object.setPrototypeOf(proxy, Object.getPrototypeOf(self));
        return proxy;
    }

    /**
     * Return the metadata that is required to register a custom Transport for Logger
     *
     * @static
     * @virtual
     * @returns {RegistrationMetadata}
     * @memberof BaseTransport
     */
    static __register() {
        throw new Error("Override the __register() function in your Transport Class");
    }
}

module.exports = BaseTransport;
