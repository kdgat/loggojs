/* eslint-disable no-console */

"use strict";

const Transport = require("./BaseTransport");
const resolveLevel = require("../LogLevels").resolve;

class ConsoleTransport extends Transport {
    log(level, source, ...message) {
        switch (level) {
            case resolveLevel("TRACE"):
                console.trace(this.formatMessage(level, source, message));
                break;
            case resolveLevel("DEBUG"):
                console.debug(this.formatMessage(level, source, message));
                break;
            case resolveLevel("INFO"):
                console.info(this.formatMessage(level, source, message));
                break;
            case resolveLevel("WARNING"):
                console.warn(this.formatMessage(level, source, message));
                break;
            case resolveLevel("ERROR"):
                console.error(this.formatMessage(level, source, message));
                break;
            default:
                console.log(this.formatMessage(level, source, message));
        }
    }

    static __register() {
        return {
            name: "console",
            type: "transport"
        };
    }
}

module.exports = ConsoleTransport;
