"use strict";

/**
 * An array that stores loglevels and is responsible for resolving them.<br>
 * <br>
 * The array has all named Loglevels and the resolve method attached to it.
 * @exports levels
 */

const LOGLEVELS = [
    "Trace",
    "Debug",
    "Info",
    "Warning",
    "Error"
];

/**
 * Takes a string and tries to resolve it to a numeric log level value based on the
 * loglevel array.
 *
 * @name resolve
 * @param {string} level The log level string that needs to be resolved to its numeric value
 * @returns {number|boolean|null} Numeric representation of the resolved loglevel or `false` if disabled. If resolution failed, returns null.
 */
function resolveLevel(val) {
    if (!isNaN(parseInt(val))) return parseInt(val);
    else if (val === false) return false;
    else if (typeof val === "string") {
        if (val.toLowerCase() === "off") return false;
        else if (LOGLEVELS.findIndex(item => item.toLowerCase() === val.toLowerCase()) !== -1) return LOGLEVELS.findIndex(item => item.toLowerCase() === val.toLowerCase());
        else if (LOGLEVELS.findIndex(item => item.toLowerCase().charAt(0) === val.toLowerCase()) !== -1) return LOGLEVELS.findIndex(item => item.toLowerCase().charAt(0) === val.toLowerCase());
        else if (LOGLEVELS.findIndex(item => item.toLowerCase().startsWith(val.toLowerCase())) !== -1) return LOGLEVELS.findIndex(item => item.toLowerCase().startsWith(val.toLowerCase()));
    }
    return null;
}

module.exports = LOGLEVELS;
module.exports.resolve = resolveLevel;
