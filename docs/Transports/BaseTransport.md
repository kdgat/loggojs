<a name="BaseTransport"></a>

## *BaseTransport ⇐ <code>ExtensionClass</code>*
**Kind**: global abstract class  
**Extends**: <code>ExtensionClass</code>  

* *[BaseTransport](#BaseTransport) ⇐ <code>ExtensionClass</code>*
    * *[new BaseTransport([level], [format])](#new_BaseTransport_new)*
    * _instance_
        * *[.level](#BaseTransport+level)*
        * *[.format](#BaseTransport+format)*
        * *[.formatMessage(level, source, data)](#BaseTransport+formatMessage) ⇒ <code>string</code>*
        * *[.canLog()](#BaseTransport+canLog) ⇒ <code>boolean</code>*
        * **[.log(level, source, message)](#BaseTransport+log) ⇒ <code>Promise</code> \| <code>undefined</code>**
        * *[.createChild()](#BaseTransport+createChild) ⇒*
    * _static_
        * **[.__register()](#BaseTransport.__register) ⇒ <code>RegistrationMetadata</code>**

<a name="new_BaseTransport_new"></a>

### *new BaseTransport([level], [format])*

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [level] | <code>string</code> \| <code>number</code> \| <code>boolean</code> | <code>false</code> | The log level that the Transporter is going to be instantiated with |
| [format] | <code>string</code> \| <code>function</code> | <code>&quot;\&quot;[{level}][{source}]{text}\&quot;&quot;</code> | The log message format of messages |

<a name="BaseTransport+level"></a>

### *baseTransport.level*
Get or set the level that the Transporter logs messages for.<br>
The log level is resolved to a number internally and returned as such.<br>
Only loglevels equal or higher than this level will get printed out.

**Kind**: instance property of [<code>BaseTransport</code>](#BaseTransport)  
<a name="BaseTransport+format"></a>

### *baseTransport.format*
The format that the log message will be printed out as.<br>
This can either be a string with variables or a function that returns a log string.

**Kind**: instance property of [<code>BaseTransport</code>](#BaseTransport)  
<a name="BaseTransport+formatMessage"></a>

### *baseTransport.formatMessage(level, source, data) ⇒ <code>string</code>*
Take in the data required to form a finished log message string and return it

**Kind**: instance method of [<code>BaseTransport</code>](#BaseTransport)  
**Returns**: <code>string</code> - The formatted string  

| Param | Type | Description |
| --- | --- | --- |
| level | <code>string</code> \| <code>number</code> | The log level that is going to be potentially inserted into the string |
| source | <code>string</code> | The source that the logger has been instantiated with |
| data | <code>string</code> | The message itself |

<a name="BaseTransport+canLog"></a>

### *baseTransport.canLog() ⇒ <code>boolean</code>*
Runtime check before logging.

**Kind**: instance method of [<code>BaseTransport</code>](#BaseTransport)  
**Returns**: <code>boolean</code> - Wether or not a log should happen with this transport  
<a name="BaseTransport+log"></a>

### **baseTransport.log(level, source, message) ⇒ <code>Promise</code> \| <code>undefined</code>**
The core function of a transport. This function is responsible to log the message to its destination.<br>
If the logging process is async, return a promise that resolve when the log has reached its target.<br>
Every transport needs to implement this function

**Kind**: instance abstract method of [<code>BaseTransport</code>](#BaseTransport)  

| Param | Type | Description |
| --- | --- | --- |
| level | <code>string</code> \| <code>number</code> | The log level of this message |
| source | <code>string</code> | The source that the logger has been instantiated with |
| message | <code>\*</code> | The data you wish to be logged |

<a name="BaseTransport+createChild"></a>

### *baseTransport.createChild() ⇒*
Creates a directly linked child.<br>
Directly linked children reflect their parents' property changes unless the child has a property overriding the parent.

**Kind**: instance method of [<code>BaseTransport</code>](#BaseTransport)  
**Returns**: Instance of [BaseTransport](#BaseTransport)  
<a name="BaseTransport.__register"></a>

### **BaseTransport.\_\_register() ⇒ <code>RegistrationMetadata</code>**
Return the metadata that is required to register a custom Transport for Logger

**Kind**: static abstract method of [<code>BaseTransport</code>](#BaseTransport)  
