## Classes

<dl>
<dt><a href="#ExtensionClass">ExtensionClass</a></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><a href="#RegistrationMetadata">RegistrationMetadata</a> : <code>Object</code></dt>
<dd></dd>
</dl>

<a name="ExtensionClass"></a>

## *ExtensionClass*
**Kind**: global abstract class  

* *[ExtensionClass](#ExtensionClass)*
    * *[new ExtensionClass()](#new_ExtensionClass_new)*
    * **[.__register()](#ExtensionClass.__register) ⇒ [<code>RegistrationMetadata</code>](#RegistrationMetadata)**

<a name="new_ExtensionClass_new"></a>

### *new ExtensionClass()*
A class that serves as the base for all Logger extension classes

<a name="ExtensionClass.__register"></a>

### **ExtensionClass.\_\_register() ⇒ [<code>RegistrationMetadata</code>](#RegistrationMetadata)**
Return the metadata that is required to register an extension for Logger

**Kind**: static abstract method of [<code>ExtensionClass</code>](#ExtensionClass)  
<a name="RegistrationMetadata"></a>

## RegistrationMetadata : <code>Object</code>
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| type | <code>string</code> | The type of the extension |
| name | <code>string</code> | The name of the extension |

