<a name="Logger"></a>

## Logger
**Kind**: global class  

* [Logger](#Logger)
    * [new Logger([source])](#new_Logger_new)
    * _instance_
        * [.transports](#Logger+transports) ⇒ <code>Object</code>
        * [.logMessage([level], ...message)](#Logger+logMessage) ⇒ <code>Promise</code>
        * [.trace(...message)](#Logger+trace) ⇒ <code>Promise</code>
        * [.debug(...message)](#Logger+debug) ⇒ <code>Promise</code>
        * [.info(...message)](#Logger+info) ⇒ <code>Promise</code>
        * [.warn(...message)](#Logger+warn) ⇒ <code>Promise</code>
        * [.error(...message)](#Logger+error) ⇒ <code>Promise</code>
        * [.log(...message)](#Logger+log) ⇒ <code>Promise</code>
    * _static_
        * [.levels](#Logger.levels) ⇒ <code>Array.&lt;string&gt;</code>
        * [.transports](#Logger.transports) ⇒ <code>Object</code>
        * [.use(extensionClass)](#Logger.use) ⇒ <code>boolean</code>

<a name="new_Logger_new"></a>

### new Logger([source])
Class that represents a single logger.<br>
**Important:** All logging functions have a static counterpart that access the global logger.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [source] | <code>string</code> | <code>&quot;\&quot;unset\&quot;&quot;</code> | The Source region this logger is used in |

<a name="Logger+transports"></a>

### logger.transports ⇒ <code>Object</code>
Return all transports assigned to this logger

**Kind**: instance property of [<code>Logger</code>](#Logger)  
**Returns**: <code>Object</code> - An object with Name-Transport Instance pairs  
**Read only**: true  
<a name="Logger+logMessage"></a>

### logger.logMessage([level], ...message) ⇒ <code>Promise</code>
Log a message to all assigned transports

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [level] | <code>string</code> \| <code>number</code> | <code>&quot;\&quot;INFO\&quot;&quot;</code> | The log level |
| ...message | <code>\*</code> |  | The text of the log message |

<a name="Logger+trace"></a>

### logger.trace(...message) ⇒ <code>Promise</code>
Log a message at _TRACE_ (0) Loglevel

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Description |
| --- | --- | --- |
| ...message | <code>\*</code> | The text of the log message |

<a name="Logger+debug"></a>

### logger.debug(...message) ⇒ <code>Promise</code>
Log a message at _DEBUG_ (1) Loglevel

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Description |
| --- | --- | --- |
| ...message | <code>\*</code> | The text of the log message |

<a name="Logger+info"></a>

### logger.info(...message) ⇒ <code>Promise</code>
Log a message at _INFO_ (2) Loglevel

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Description |
| --- | --- | --- |
| ...message | <code>\*</code> | The text of the log message |

<a name="Logger+warn"></a>

### logger.warn(...message) ⇒ <code>Promise</code>
Log a message at _WARNING_ (3) Loglevel

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Description |
| --- | --- | --- |
| ...message | <code>\*</code> | The text of the log message |

<a name="Logger+error"></a>

### logger.error(...message) ⇒ <code>Promise</code>
Log a message at _ERROR_ (4) Loglevel

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Description |
| --- | --- | --- |
| ...message | <code>\*</code> | The text of the log message |

<a name="Logger+log"></a>

### logger.log(...message) ⇒ <code>Promise</code>
Log a message at -1 Loglevel

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>Promise</code> - Promise that resolves as soon as the log message queue is empty  

| Param | Type | Description |
| --- | --- | --- |
| ...message | <code>\*</code> | The text of the log message |

<a name="Logger.levels"></a>

### Logger.levels ⇒ <code>Array.&lt;string&gt;</code>
Return an array of available named loglevels<br>
Add, remove or reorganize loglevels in this Array to change the available log level names.
See [levels](module-levels.html)

**Kind**: static property of [<code>Logger</code>](#Logger)  
**Returns**: <code>Array.&lt;string&gt;</code> - The loglevel array and resolve function  
**Read only**: true  
<a name="Logger.transports"></a>

### Logger.transports ⇒ <code>Object</code>
Access the globally registered Transporters

**Kind**: static property of [<code>Logger</code>](#Logger)  
**Returns**: <code>Object</code> - An object with Name-Transport Instance pairs  
**Read only**: true  
<a name="Logger.use"></a>

### Logger.use(extensionClass) ⇒ <code>boolean</code>
Register new extensions to Logger.<br>
Currently only supports Transports.

**Kind**: static method of [<code>Logger</code>](#Logger)  
**Returns**: <code>boolean</code> - True on successful registering, false otherwise  

| Param | Type | Description |
| --- | --- | --- |
| extensionClass | <code>ExtensionClass</code> | Class, that extends Loggo in some shape or form |

