<a name="module_levels"></a>

## levels
An array that stores loglevels and is responsible for resolving them.<br>
<br>
The array has all named Loglevels and the resolve method attached to it.

<a name="module_levels..resolve"></a>

### levels~resolve ⇒ <code>number</code> \| <code>boolean</code> \| <code>null</code>
Takes a string and tries to resolve it to a numeric log level value based on the
loglevel array.

**Kind**: inner property of [<code>levels</code>](#module_levels)  
**Returns**: <code>number</code> \| <code>boolean</code> \| <code>null</code> - Numeric representation of the resolved loglevel or `false` if disabled. If resolution failed, returns null.  

| Param | Type | Description |
| --- | --- | --- |
| level | <code>string</code> | The log level string that needs to be resolved to its numeric value |

