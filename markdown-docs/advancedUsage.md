# Advanced Usage
## Add your own Transport to a Logger
```js
const newTransport = new Transport;
lg.transports["newTransportName"] = newTransport;
```
## Access global Transports
These transports get linked to as children for every new Logger that is created.
```js
Logger.transports;
```
## Register global Transport
A transport needs to have a `__register` function in order to be a valid global Transport.<br>
The function must return [valid Extension Metadata](global.html#RegistrationMetadata) with a type of `"transport"`.<br>
You can still use Transports without this function as local Transports.
```js
const newGlobalLogger = require("./newGlobalLogger.js")
Logger.use(newGlobalLogger);
```
## Format function
Set the format property of a Transport to a function instead of a string to have it evaluated.
```js
// The format object has the following properties
const formatObject = {
    level,      // Numeric Loglevel value
    text,       // The log message
    source,     // The source the Logger has been created with
    y,          // Year
    m,          // Month
    d,          // Day
    h,          // Hour
    i,          // Minute
    s,          // Second
    ms,         // Millisecond
    z           // Timezone Offset
}
Transport.format = function(formatObject) {
    return "Some New Value";
}
```
## Custom Loglevel
Use a custom level when doing logging
```js
// Level can either be numeric or a Levelstring
lg.logMessage(level, ...message)
```
## Create a linked child of a Transport
```js
lg.transports.console.level = 4;

const child = lg.transports.console.createChild();

lg.transports.console.level;
> 4
child.level;
> 4

lg.transports.console.level = 2;
child.level
> 2

child.level = 1;
lg.transports.console.level = 3;
child.level;
> 1;

delete child["level"];
child.level;
> 3
```
