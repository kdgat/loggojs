# Custom Transport
A custom Transport extends Loggo's BaseTransport class and implements an async (returns a Promise) function `log()`
```js
const BaseTransport = require("Loggo/BaseTransport");

class MySimpleTransport extends BaseTransport {
    async log(level, source, ...message) {
        // Output the log somewhere, for example a file
    }
}
```

Transports support multiple special methods.
```js
class MyComplexTransport extends MySimpleTransport {
    // Decide during runtime if this Transport should do logging, for example
    // you can check if this Transport is used in a main or a render process of electron
    // and decide based on that
    canLog() {
        if (runtimeLogCondition) return true;
        else return false;
    }

    // This property can either be a function or a static Object
    // It is required for a global Transport
    static __register() {
        return {
            type: "transport",
            name: "tComplex"
        };
    }
}
```
