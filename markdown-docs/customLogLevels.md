# Custom Loglevels
LogGo allows custom named loglevels, allowing easy adoption of multiple loglevel standards.<br>
All loglevel names are handled through a central array, accessible through `Logger.levels`.

```js
Logger.levels;
> ["Trace", "Debug", "Info", "Warning", "Error"]
```

Modifying this array will immediately impact any further name resolution.<br>
The levels in the Array are ordered from most verbose to least verbose level.
## The `resolve` function
The levels array has a special function called `resolve` attached to it that can take a log level name and resolve it to its respective numeric verbosity value.
```js
Logger.levels.resolve("INFO");
> 2
```

## Introducing custom log levels
### Add an even more verbose level
```js
Logger.levels.unshift("Silly");
Logger.levels;
> ["Silly", "Trace", "Debug", "Info", "Warning", "Error"]
Logger.levels.resolve("INFO");
> 3
```
### Add a least verbose level
```js
Logger.levels.push("Emergency");
Logger.levels;
> ["Trace", "Debug", "Info", "Warning", "Error", "Emergency"]
Logger.levels.resolve("INFO");
> 2
```
### Insert a new level between other levels
```js
Logger.levels.splice(2, 0, "Fluff");
Logger.levels;
> ["Trace", "Debug", "Fluff", "Info", "Warning", "Error"]
Logger.levels.resolve("INFO");
> 3
```
