# Changelog
## 1.0.3
### Released 01/22/2019
```diff
* Fix children losing their getter/setters
```
## 1.0.2
### Released 01/21/2019
```diff
+ Add unit tests and linting
* Fix createChildren() instanceof returning wrong prototype
* Fix Loglevels.resolve() documentation
```
## 1.0.1
### Released: 01/16/2019
```diff
* Fixed global Logger not having any transports
```
## 1.0.0
### Released: 01/16/2019
```diff
* Initial release
```
