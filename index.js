"use strict";

const Logger = require("./lib/Logger");

const ConsoleTransport = require("./lib/Transports/ConsoleTransport");

Logger.use(ConsoleTransport);

module.exports = Logger;
